import java.io.ObjectInputStream;
import java.io.FileInputStream;

public class Deserialization{
    public static void main(String args[]) throws Exception{
	ObjectInputStream ois = new ObjectInputStream(new FileInputStream("./Person.bin"));
        Person p = (Person)ois.readObject();

	System.out.println("Name:\t\t" + p.name + "\nBirthDate:\t\t" + p.birthDate);
    }

}
