import java.util.Date;
import java.io.ObjectOutputStream;
import java.io.FileOutputStream;

public class Serialization{
    public static void main(String args[]) throws Exception{
        Person p = new Person();
	p.name = "caiqiqi";
	p.birthDate = new Date(0x1337);

	ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./Person.bin"));
	oos.writeObject(p);
	oos.flush();
    }

}
